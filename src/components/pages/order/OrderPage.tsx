import {observer} from "mobx-react";
import React from "react";
import {AppContext} from "../../../stores/Stores";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import {Map as LeafletMap, TileLayer, Marker, Popup} from 'react-leaflet';
import Taxi from '@material-ui/icons/LocalTaxi';
import {Crew} from '../../../types/Crew';
import L from 'leaflet';

export const OrderPage = observer(() => {
    const {orderStore} = React.useContext(AppContext);
    const errorMarker = new L.Icon({
        iconUrl: 'https://nipc.gov.ng/wp-content/uploads/2019/01/Location-512.png',
        iconSize: new L.Point(50, 50),
    });

    return (
        <>
            <div style={{width: 800, margin: '0 auto'}}>
                <div className="container">
                    <div className="row">
                        <div className="col mt-5">
                            <h2>Детали заказа</h2>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col mt-4">
                            <div>
                                {(orderStore.hasErrors) && (
                                    <label style={{color: 'red'}}>Адрес не найден</label>
                                )}
                                <TextField
                                    value={orderStore.address}
                                    onChange={e => {
                                        orderStore.getAddressByValue(e.target.value);
                                    }}
                                    style={{width: '100%'}}
                                    placeholder="Откуда"/>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {(orderStore.crews.length > 0) && (
                            <div
                                onClick={() => {
                                    orderStore.setCrew(orderStore.crews[0].crew_id)
                                }}
                                className="col mt-4">
                                <label style={{
                                    width: '100%',
                                    background: orderStore.order.crew_id == orderStore.crews[0].crew_id
                                        ? 'rgb(209, 220, 230)' : 'rgb(232, 232, 232)',
                                    cursor: 'pointer'
                                }}
                                       className="p-3">
                                    <Taxi style={{fontSize: 22}}/>
                                    &nbsp;подходящий экипаж :
                                    &nbsp;{orderStore.crews[0].car_mark}
                                    &nbsp;{orderStore.crews[0].car_model}
                                    &nbsp;{orderStore.crews[0].car_number}
                                    &nbsp;{orderStore.crews[0].car_color}
                                </label>
                            </div>
                        )}
                    </div>
                    <div className="row mt-4">
                        <div className="col-7">
                            <LeafletMap
                                center={[56.855532, 53.217462]}
                                zoom={12}
                                maxZoom={15}
                                attributionControl={true}
                                zoomControl={true}
                                doubleClickZoom={true}
                                scrollWheelZoom={true}
                                dragging={true}
                                animate={true}
                                easeLinearity={0.35}
                                onClick={(e: any) => {
                                    orderStore.getAddressByCoords(e.latlng.lat, e.latlng.lng);
                                }}>
                                <TileLayer
                                    url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
                                />
                                {(orderStore.coords.isValid) && (
                                    <Marker
                                        position={[orderStore.coords.lat, orderStore.coords.lon]}>
                                        <Popup>
                                            Заказ
                                        </Popup>
                                    </Marker>
                                )}
                                {(!orderStore.coords.isValid) && (
                                    <Marker
                                        position={[orderStore.coords.lat, orderStore.coords.lon]}
                                        icon={errorMarker}>
                                        <Popup>
                                            Ошибка местоположения
                                        </Popup>
                                    </Marker>
                                )}
                                {orderStore.crews.map((crew: Crew, i) => (
                                    <Marker key={i} position={[crew.lat, crew.lon]}>
                                        <Popup>
                                            {crew.driver_name}
                                        </Popup>
                                    </Marker>
                                ))}
                            </LeafletMap>
                        </div>
                        <div className="col-5">
                            {orderStore.crews.map((crew: Crew, i) => (
                                <div key={i}>
                                    {(i > 0) && (
                                        <div className="d-flex p-2 mt-2"
                                             onClick={() => {
                                                 orderStore.setCrew(crew.crew_id)
                                             }}
                                             style={{
                                                 background: orderStore.order.crew_id == crew.crew_id
                                                     ? 'rgb(209, 220, 230)' : 'rgb(232, 232, 232)',
                                                 cursor: 'pointer'}}>
                                        <div style={{width: '70%'}}>
                                            <div style={{width: '100%'}}>
                                                <Taxi style={{fontSize: 22}}/> {crew.car_mark} {crew.car_model}
                                            </div>
                                            <div style={{marginLeft: 27}}>
                                                {crew.car_color}
                                            </div>
                                        </div>
                                        <div className="float-right text-right" style={{width: '30%'}}>
                                            {crew.distance} м
                                        </div>
                                    </div>)}
                                </div>
                            ))}
                            <Button
                                style={{
                                    width: '100%',
                                    borderRadius: '0px',
                                    background: 'white',
                                    border: '1px solid',
                                    boxShadow: 'none',
                                    color: orderStore.hasErrors ? 'grey' : 'black'
                                }}
                                onClick={() => {
                                    orderStore.sendOrder()
                                }}
                                className="mt-3">
                                ЗАКАЗАТЬ
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
});