export default interface Coords {
    lat: number,
    lon: number,
    isValid: boolean
}