export default interface Address {
    address: string,
    lat: number
    lon: number
}