import Address from './Address'

export default interface Order {
    source_time: string,
    addresses: Address[],
    crew_id: number
}