import React from 'react';
import { OrderPage } from "./components/pages/order/OrderPage";

const App: React.FC = () => {
  return (
     <OrderPage/>
  );
};

export default App;
