import { Server } from "miragejs"
import Order from '../types/Order'
import {CrewSearch} from '../types/Crew'

class OrderService {
    dadataURL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/';

    constructor() {
        new Server({
            routes() {
                this.namespace = "/api";
                this.passthrough('https://suggestions.dadata.ru/**');
                this.post("/crews", () => {
                    return {
                        code: 0,
                        descr: "OK",
                        data: {
                            crews_info: [
                                {
                                    crew_id: 123,
                                    car_mark: "Chevrolet",
                                    car_model: "Lacetti",
                                    car_color: "синий",
                                    car_number: "е234ку",
                                    driver_name: "Деточкин",
                                    driver_phone: "7788",
                                    lat: 56.855532,
                                    lon: 53.217462,
                                    distance: 300
                                }, {
                                    crew_id: 125,
                                    car_mark: "Hundai",
                                    car_model: "Solaris",
                                    car_color: "белый",
                                    car_number: "ф567ас",
                                    driver_name: "Петров",
                                    driver_phone: "8899",
                                    lat: 56.860581,
                                    lon: 53.209223,
                                    distance: 600
                                }, {
                                    crew_id: 126,
                                    car_mark: "Lada",
                                    car_model: "Granta",
                                    car_color: "черный",
                                    car_number: "ф555ас",
                                    driver_name: "Петров",
                                    driver_phone: "8899",
                                    lat: 56.85103712159081,
                                    lon: 53.209224,
                                    distance: 800
                                }
                            ]
                        }
                    };
                });
                this.post("/orders", () => {
                    return {
                        code: 0,
                        descr: "OK",
                        data: {
                            order_id: 12345
                        }
                    };
                })
            },
        })
    }

    async getAddress(value: string) {
        try {
            const response = await fetch(`${this.dadataURL}suggest/address`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': "Token 972f7a40e88470013cd7554f68a328625a0fc5ab",
                },
                body: JSON.stringify({
                    "query": value, "count": 1
                }),
            });

            return await response.json();
        } catch (error) {
            throw error
        }
    }

    async getAddressByCoords(lat: number, lon: number) {
        try {
            const response = await fetch(`${this.dadataURL}geolocate/address`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': "Token 972f7a40e88470013cd7554f68a328625a0fc5ab",
                },
                body: JSON.stringify({
                    lat, lon
                }),
            });

            return await response.json();
        } catch (error) {
            throw error
        }
    }

    async getCrews(crewSearch: CrewSearch) {
        try {
            const response = await fetch(`/api/crews`, {
                method: 'POST',
                body: JSON.stringify(crewSearch),
            });

            return await response.json();
        } catch (error) {
            throw error
        }
    }

    async sendOrder(order: Order) {
        try {
            const response = await fetch(`/api/orders`, {
                method: 'POST',
                body: JSON.stringify(order),
            });

            return await response.json();
        } catch (error) {
            throw error
        }
    }
}

export const orderService = new OrderService();