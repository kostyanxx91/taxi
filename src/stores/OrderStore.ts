import {observable, action, toJS} from 'mobx';
import Order from '../types/Order'
import Coords from '../types/Coords'
import {Crew, CrewSearch} from '../types/Crew'
import {orderService} from '../services/OrderService';

class OrderStore {
    @observable order: Order = <Order>{};
    @observable crews: Crew[] = [];
    @observable address: string = "";
    @observable coords: Coords = {lat: 0, lon: 0, isValid: true};
    @observable hasErrors: boolean = false;

    @action
    sendOrder = async () => {
        if (!this.order.crew_id || !this.coords.isValid) {
            this.hasErrors = true;
        }
        if (!this.hasErrors && this.address.length > 0) {
            this.order.source_time = this.getSorceTime();
            this.order.addresses = [{
                address: this.address,
                lat: this.coords.lat,
                lon: this.coords.lon,
            }];

            orderService.sendOrder(this.order);
        }
    };

    @action
    setCrew = async (id: number) => {
        this.order.crew_id = id;
    };

    @action
    async getAddressByValue(value: string) {
        this.address = value;
        this.coords = {lat: 0, lon: 0, isValid: true};
        const addresses = await orderService.getAddress(`Ижевск, ${value}`);
        await this.setAddress(addresses);
    }

    @action
    async getAddressByCoords(lat: number, lon: number) {
        const addresses = await orderService.getAddressByCoords(lat, lon);
        this.coords = {
            lat,lon,isValid: true
        };
        await this.setAddress(addresses);
    }

    setAddress = async (addresses: any) => {
        if (addresses['suggestions'] && addresses['suggestions'].length>0) {
            const suggestion = addresses['suggestions'][0].data;
            if (suggestion['house'] && suggestion['house']!=null) {
                this.hasErrors = false;
                this.coords = {
                    lat: suggestion.geo_lat,
                    lon: suggestion.geo_lon,
                    isValid: true
                };
                this.address = `${suggestion['street']}, ${suggestion['house']}`;
                const crewSearch: CrewSearch = {
                    source_time: this.getSorceTime(),
                    addresses: [{
                        address: `${suggestion.street} ${suggestion.house}`,
                        lat: suggestion.geo_lat,
                        lon: suggestion.geo_lon
                    }]
                };
                const crews = await orderService.getCrews(crewSearch);
                if (crews['data'] && crews['data']['crews_info']) {
                    if (crews['data']['crews_info'].length>0) {
                        this.order.crew_id = crews['data']['crews_info'][0].crew_id;
                    }

                    this.crews = crews['data']['crews_info'];
                }
            } else {
                this.resetForm();
                this.coords.isValid = false;
            }
        } else {
            this.resetForm();
            this.coords.isValid = false;
        }
    };

    resetForm = () => {
        this.crews = [];
    };

    pad = (value: number) => {
        if (value<=9) {
            return "0" + value.toString();
        }

        return value.toString();
    };

    getSorceTime = () => {
        const date = new Date();

        return this.pad(date.getFullYear())
            +this.pad(date.getMonth()+1)
            +this.pad(date.getDay())
            +this.pad(date.getHours())
            +this.pad(date.getMinutes())
            +this.pad(date.getSeconds());
    }
}

export default new OrderStore();