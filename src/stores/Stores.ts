import * as React from "react";
import orderStore from './OrderStore';

export function createStores() {
    return {
        orderStore,
    };
}

export const stores = createStores();

export const AppContext = React.createContext(stores);